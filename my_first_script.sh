Create a new file called hello.sh using a text editor such as nano or vi:
nano hello.sh
Add the following code:
#!/bin/bash
echo "Hello World"
Set the script executable permission by running chmod command:
chmod +x hello.sh
Run or execute the script using following syntax:
sh hello.sh
